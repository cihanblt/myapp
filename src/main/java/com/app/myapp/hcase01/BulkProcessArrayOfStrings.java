package com.app.myapp.hcase01;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BulkProcessArrayOfStrings {

    private static final int GROUP_LIMIT = 3;
    private static final int SUMMMING_THRESHOLD = 90;


    public int[] parseListOfStrings(String[][] listOfString) {
        ArrayList<String> strList = getFlattenedStrings(listOfString);
        System.out.println(strList);

        List<Integer> intList = eliminateLettersAndConvertToNumber(strList);

        List<List<Integer>> collect = createIntegerSublists(intList);

        int[] eliminatedInts = chooseThatProvideTheCase(collect);

        return eliminatedInts;
    }

    private int[] chooseThatProvideTheCase(List<List<Integer>> collect) {
        return collect
                .stream()
                .filter(list -> list.stream().collect(Collectors.summarizingInt(Integer::intValue)).getSum() > SUMMMING_THRESHOLD)
                .flatMap(integers -> integers.stream())
                .mapToInt(Integer::intValue)
                .toArray();
    }

    private List<List<Integer>> createIntegerSublists(List<Integer> intList) {
        return IntStream
                    .range(0, intList.size())
                    .filter(i -> i % GROUP_LIMIT == 0)
                    .mapToObj(i -> {
                        if (i < intList.size() - GROUP_LIMIT) {
                            return intList.subList(i, i + GROUP_LIMIT);
                        } else {
                            return intList.subList(0,0);
                        }
                    })
                    .collect(Collectors.toList());
    }

    private List<Integer> eliminateLettersAndConvertToNumber(ArrayList<String> strList) {
        return strList.stream()
                    .filter(s -> isNumeric(s))
                    .map(s -> Integer.valueOf(s))
                    .collect(Collectors.toList());
    }

    private ArrayList<String> getFlattenedStrings(String[][] listOfString) {
        var strList = new ArrayList<String>();
        for (String[] strings : listOfString) {
            strList.addAll(Arrays.asList(strings));
        }
        return strList;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        String[][] arr = {
                {"0", "s1", null, "35", "90", "60"},
                {"ttt", null, null, "15",},
                {"75", "95", "0", "0", null, "ssss", "0", "-15"},
                {"25", "fgdfg", "", "105", "dsfdsf", "-5"}
        };

        BulkProcessArrayOfStrings bulkProcessArrayOfStrings = new BulkProcessArrayOfStrings();
        int[] ints = bulkProcessArrayOfStrings.parseListOfStrings(arr);
        IntStream
                .range(0 , ints.length)
                .forEach(inx -> System.out.print(ints[inx] + " "));
    }
}
