package com.app.myapp.hcase02.mapper;


import com.app.myapp.hcase02.DataSetDto;
import com.app.myapp.hcase02.model.DataSet;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DataSetMapper {

    DataSetDto toDto(DataSet dataSet);

    DataSet toEntity(DataSetDto dataSetDto);



}
