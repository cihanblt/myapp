package com.app.myapp.hcase02;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue dataSetQueue() {
        return new Queue("dataSet", true);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange("direct-exchange");
    }

    @Bean
    Binding testeBinding(Queue dataSetQueue, DirectExchange exchange) {
        return BindingBuilder.bind(dataSetQueue).to(exchange).with("dataSet-routing-key");
    }


}
