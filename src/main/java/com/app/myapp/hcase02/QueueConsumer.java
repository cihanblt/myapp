package com.app.myapp.hcase02;


import com.app.myapp.hcase02.mapper.DataSetMapper;
import com.app.myapp.hcase02.model.DataSet;
import com.app.myapp.hcase02.repository.DataSetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
@RequiredArgsConstructor
public class QueueConsumer {

    private final DataSetRepository dataSetRepository;

    private final DataSetMapper dataSetMapper;

    @RabbitListener(queues = {"${queue.name}"})
    public void receive(@Payload Message message) {
        System.out.println("Message " + message + "  " + LocalDateTime.now());
        DataSetDto payload = (DataSetDto) message.getPayload();
        DataSet dataSet = dataSetMapper.toEntity(payload);
        dataSetRepository.save(dataSet);
        System.out.println("payload : " + payload);
    }

}