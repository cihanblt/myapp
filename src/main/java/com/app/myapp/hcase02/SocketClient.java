package com.app.myapp.hcase02;

import org.springframework.util.DigestUtils;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalTime;
import java.util.Random;

public class SocketClient {

    private Socket clientSocket;

    public void startConnection(String ip, int port) throws Exception {
        clientSocket = new Socket(ip, port);

    }

    public static void main(String[] args) {
        SocketClient socketClient = new SocketClient();
        try {
            while (true) {
                socketClient.startConnection("localhost", 6666);
                DataSetDto dataSetDto = new DataSetDto();
                ObjectOutputStream os = new ObjectOutputStream(socketClient.clientSocket.getOutputStream());
                dataSetDto.setTimestamp(LocalTime.now());
                dataSetDto.setNumberValue(new Random().nextInt(100));
                String digestAsHex = DigestUtils.md5DigestAsHex(String.valueOf(dataSetDto.getTimestamp()).concat(String.valueOf(dataSetDto.getNumberValue())).getBytes());
                dataSetDto.setHashedValue(digestAsHex);

                os.writeObject(dataSetDto);
                ObjectInputStream is = new ObjectInputStream(socketClient.clientSocket.getInputStream());
                DataSetDto returnMessage = (DataSetDto) is.readObject();
                System.out.println("return Message is=" + returnMessage);
                Thread.sleep(1000l);
                is.close();
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
