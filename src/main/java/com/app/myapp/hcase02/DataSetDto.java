package com.app.myapp.hcase02;

import lombok.*;

import java.io.Serializable;
import java.time.LocalTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataSetDto implements Serializable {

    private static final long serialVersionUID = -5399605122490343339L;

    private LocalTime timestamp;
    private int numberValue;
    private String hashedValue;

}
