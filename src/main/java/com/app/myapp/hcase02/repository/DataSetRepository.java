package com.app.myapp.hcase02.repository;

import com.app.myapp.hcase02.model.DataSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataSetRepository extends JpaRepository<DataSet, Long> {
}
