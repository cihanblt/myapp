package com.app.myapp.hcase02;

public class BusinessException extends Exception{
    public BusinessException(String message) {
        super(message);
    }
}