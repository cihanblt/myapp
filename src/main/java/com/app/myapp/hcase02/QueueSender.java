package com.app.myapp.hcase02;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QueueSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    private Queue queue;

    public void send(DataSetDto dataSetDto) {
        rabbitTemplate.convertAndSend(this.queue.getName(), dataSetDto);
    }
}