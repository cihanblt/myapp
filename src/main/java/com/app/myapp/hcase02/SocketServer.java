package com.app.myapp.hcase02;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer implements AutoCloseable {
    private ServerSocket serverSocket;
    private Socket clientSocket;

    public SocketServer(int port) throws Exception {
        serverSocket = new ServerSocket(port);
    }

    public DataSetDto start() throws Exception {
        clientSocket = serverSocket.accept();
        ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());
        ObjectInputStream is = new ObjectInputStream(clientSocket.getInputStream());

        DataSetDto m = (DataSetDto) is.readObject();
        os.writeObject(m);
        return m;
    }


    @Override
    public void close() throws Exception {
        clientSocket.close();
        serverSocket.close();
    }
}
