package com.app.myapp;

import com.app.myapp.hcase02.DataSetDto;
import com.app.myapp.hcase02.QueueSender;
import com.app.myapp.hcase02.SocketServer;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class MyAppApplication implements ApplicationRunner {

    @Autowired
    private QueueSender queueSender;

    public static void main(String[] args) {
        SpringApplication.run(MyAppApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args){
        try (SocketServer server = new SocketServer(6666)) {
            while (true) {
                DataSetDto dataSetDto = server.start();
                queueSender.send(dataSetDto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
