package com.app.myapp.hcase01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BulkProcessArrayOfStringsTest {

    @Test
    void parseListOfStrings() {
        String[][] arr = {
                {"0", "s1", null, "35", "90", "60"},
                {"ttt", null, null, "15",},
                {"75", "95", "0", "0", null, "ssss", "0", "-15"},
                {"25", "fgdfg", "", "105", "dsfdsf", "-5"}
        };

        BulkProcessArrayOfStrings bulkProcessArrayOfStrings = new BulkProcessArrayOfStrings();
        Assertions.assertEquals(9, bulkProcessArrayOfStrings.parseListOfStrings(arr).length);
    }
}